<!DOCTYPE html>
<html lang="">
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<title>Basicwebsite</title>
		<!-- Bootstrap CSS -->
		<link rel="stylesheet" href="/css/app.css">
	</head>
	<body>
		@include('inc.navbar')
		<div class="container">
			@if(Request::is('/'))
				@include('inc.showcase')
			@endif
			<div class="row">
				<div class="col-md-8" id="main-section">
					@include('inc.alerts')
					@yield('content')
				</div>
				<div class="col-md-4" id="sidebar-section">
					@include('inc.sidebar')
				</div>
			</div>
		</div>
		<footer id="footer" class="text-center">
			<p>Copyright 2017 &copy; .:ESP:.</p>
		</footer>
		
		<script src="/js/app.js"></script>
		<!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
 		<script src="Hello World"></script>
	</body>
</html>