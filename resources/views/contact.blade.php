@extends('layouts.app')

@section('content')
	<h1>Contact us</h1>
	{!! Form::open(['url' => 'contact/submit', 'method' => 'POST']) !!}
	    <div class="form-group">
	    	{{ Form::label('name', 'Name') }}
	    	{{ Form::text('name', '', ['class'=>'form-control', 'placeholder'=>'John Doe'])}}
	    </div>
	    <div class="form-group">
	    	{{ Form::label('email', 'E-Mail Address') }}
	    	{{ Form::text('email', '', ['class'=>'form-control', 'placeholder'=>'example@gmail.com'])}}
	    </div>
	    <div class="form-group">
	    	{{ Form::label('message', 'Your message') }}
	    	{{ Form::textarea('message', '', ['class'=>'form-control'])}}	    </div>
	    <div class="form-group text-right">
	    	{{ Form::submit('Send!', ['class'=>'btn btn-primary']) }}
	    </div>
	{!! Form::close() !!}
@endsection