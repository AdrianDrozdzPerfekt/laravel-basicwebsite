<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Message;

class MessagesController extends Controller
{
    public function submit(Request $request)
    {
    	//$this->validate(WHAT, RULES, MESSAGES)
    	$this->validate($request, [
    		//rules
    		// nazwa_pola => regula1|regula2....,
    		'name' => 'required',
    		'email' => 'required|email',
    		'message' => 'required|min:10',
		],[
			//'pole.regula' => 'Tresc'
			'name.required' => 'Proszę podać imię i nazwisko',
			'email.required' => 'Proszę podać e-mail',
			'email.email' => 'Proszę podać poprawny adres e-mail',
			'message.required' => 'Proszę podać treść wiadomości',
			'message.min' => 'Wiadomość za krótka',
		]);

    	//save message in db
        $message = new Message();
        $message->name = $request->input('name');
        $message->email = $request->input('email');
        $message->message = $request->input('message');
        $message->save();

    	//return to homepage with alert success
    	return redirect('/')
    			->with('success', 'Wiadomość wysłana!');	
    }

    public function getMessages()
    {
        $messages = Message::all();

        return view('messages')->with('messages', $messages);
    }
}
